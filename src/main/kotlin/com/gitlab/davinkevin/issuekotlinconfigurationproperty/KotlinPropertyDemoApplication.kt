package com.gitlab.davinkevin.issuekotlinconfigurationproperty

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty
import org.springframework.boot.runApplication
import java.net.URI

@SpringBootApplication
@EnableConfigurationProperties(FooProperties::class)
class KotlinPropertyDemoApplication

fun main(args: Array<String>) {
	runApplication<KotlinPropertyDemoApplication>(*args)
}

@ConstructorBinding
@ConfigurationProperties("foo")
data class FooProperties(
		/**
		 * URL of the foo component
		 */
		val uri: URI,
		/**
		 * Authentication configuration
		 */
		@NestedConfigurationProperty
		val auth: AuthenticateProperties
)

@ConstructorBinding
data class AuthenticateProperties (
		/**
		 * The url of the authenticate component
		 */
		val url: URI,
		/**
		 * Method 1 of auth
		 */
		@NestedConfigurationProperty
		val one: Method1 = Method1(),
		/**
		 * Method 2 of auth
		 */
		@NestedConfigurationProperty
		val two: Method2 = Method2(),
		/**
		 * Method 3 of auth
		 */
		@NestedConfigurationProperty
		val three: Method3 = Method3()
)

@ConstructorBinding
data class Method1(
		/**
		 * key used for method 1
		 */
		val key: String = ""
)

@ConstructorBinding
data class Method2(
		/**
		 * token used for method 2
		 */
		val token: String = ""
)

@ConstructorBinding
data class Method3 (
		/**
		 * keyid
		 */
		val keyId: String = "",
		/**
		 * key
		 */
		val key: String = ""
)
